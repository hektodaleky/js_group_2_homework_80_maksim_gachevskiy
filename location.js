const express = require('express');
const router = express.Router();


const createRouter = (db) => {

    router.get('/:id', (req, res) => {

        db.query('select* from office.location where id=' + req.params.id,
            function (error, results, fields) {
                if (error) res.status(400);
                res.send(results);
            })


    });

    router.get('/', (req, res) => {

        db.query('select* from office.location',
            function (error, results, fields) {
                if (error) throw error;
                res.send(results);
            })

    });
    router.post('/', (req, res) => {
        const itemBody = req.body;
        if (!itemBody.name) {
            res.status(400).send('Location.name ERROR');
            return
        }

        db.query('insert into office.location (name,about) Values (?,?)',
            [itemBody.name, itemBody.about],
            function (error, results, fields) {
                if (error) throw error;
                res.send(results);
            })

    });

    router.delete('/:id', (req, res) => {
        db.query(`delete from office.location where id=${req.params.id}`,
            function (error, results, fields) {
                if (error) res.status(404).send("ПРоблема");
                res.send(results);
            });
    });

    router.put('/:id', (req, res) => {
        const itemBody = req.body;
        let query = [];
        if (itemBody.name) {
            query.push(` name = "${itemBody.name}" `)
        }
        if (itemBody.about) {
            query.push(` about = "${itemBody.about}" `)
        }

        console.log(query);
        db.query(`update office.location
         set ${query.join(', ')}
         where id=${req.params.id}`,
            function (error, results, fields) {
                if (error) res.status(404).send("ПРоблема");
                res.send(results);
            });
    });


    return router;

};

module.exports = createRouter;