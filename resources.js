const express = require('express');
const multer = require('multer');
const path = require('path');
const config = require('./config');
const router = express.Router();
const nanoid = require('nanoid');

const storage = multer.diskStorage({
    destination: (req, file, cd) => {
        cd(null, config.uploadPath);
    }, filename: (req, file, cd) => {
        cd(null, nanoid() + path.extname(file.originalname))
    }
});

const upload = multer({storage});


const createRouter = (db) => {

    router.get('/:id', (req, res) => {

        db.query('select* from office.subject_of_accounting where id=' + req.params.id,
            function (error, results, fields) {
                if (error) res.status(400);
                res.send(results);
            })


    });

    router.get('/', (req, res) => {

        db.query('select* from office.subject_of_accounting',
            function (error, results, fields) {
                if (error) throw error;
                res.send(results);
            })

    });
    router.post('/', upload.single('image'), (req, res) => {
        const itemBody = req.body;
        console.log(itemBody);
        if (req.file) {
            itemBody.image = req.file.filename;
        }
        else {
            itemBody.image = null
        }
        db.query('insert into office.subject_of_accounting (name,category_id,location_id,about,image) Values (?,?,?,?,?)',
            [itemBody.name, itemBody.category, itemBody.location, itemBody.about, itemBody.image],
            function (error, results, fields) {
                if (error) throw error;
                res.send(results);
            })

    });

    router.delete('/:id', (req, res) => {
        db.query(`delete from office.subject_of_accounting where id=${req.params.id}`,
            function (error, results, fields) {
                if (error) res.status(404).send("ПРоблема");
                res.send(results);
            });
    });

    router.put('/:id', (req, res) => {
        console.log("PUT");

        const itemBody = req.body;
        let query = [];
        if (itemBody.name) {
            query.push(` name = "${itemBody.name}" `)
        }
        if (itemBody.category) {
            query.push(` category = "${itemBody.category}" `)
        }
        if (itemBody.about) {
            query.push(` about = "${itemBody.about}" `)
        }

        console.log(query);
        db.query(`update office.subject_of_accounting
         set ${query.join(', ')}
         where id=${req.params.id}`,
            function (error, results, fields) {
                if (error) res.status(404).send("ПРоблема");
                res.send(results);
            });
    });


    return router;

};

module.exports = createRouter;