const express = require('express');
const mysql = require('mysql');


const app = express();
app.use(express.json());
const resourсes = require('./resources');
const location = require('./location');
const categories = require('./categories');
app.use(express.static('public'));
const port = 8000;


const con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "carvet",
    database: 'office'
});


con.connect((err) => {
    if (err) throw err;
    app.use('/resoureces', resourсes(con));
    app.use('/location', location(con));
    app.use('/categories', categories(con));
    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });
});